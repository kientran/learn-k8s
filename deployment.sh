#!/bin/bash
NAMESPACE=test
for file in components/*
do
    echo "Processing: $file"
    kubectl apply -f "${file}"
done