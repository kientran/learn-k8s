#!/bin/bash
AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}

cleanup() {
    repoURI=$1
    numbck=$2

    repository=$(echo "${repoURI}" | sed -e 's/.*\.ecr\.\([-a-z0-9]\+\)\.amazonaws\.com\///g')
    region=$(echo "${repoURI}" | sed -e 's/.*\.ecr\.\([-a-z0-9]\+\)\.amazonaws\.com.*$/\1/g')

    # Start process
    echo "[cleanup] Number images to keep: $numbck"
    echo "[cleanup] [aws][ecs][describe-images] ${repository}"
    listImages=$(aws ecr describe-images --region $region --repository-name $repository --output json --query 'sort_by(imageDetails,& imagePushedAt)')
    numImages=$(echo $listImages | jq length)
    numImages=${numImages:-0}

    echo "[cleanup] Images in repository: $numImages" 

    listImageRemove=()
    while read -r img; do
        if [ "$numImages" -gt "$numbck" ]; then
            imageDigest=$(echo $img | jq -c '.imageDigest')
            imageTags=$(echo $img | jq -c '.imageTags')

            # ingore if that's an prod or latest tag
            if [[ $imageTags == *"prod"* || $imageTags == *"latest"* ]]; then
                continue
            else
                listImageRemove[${#listImageRemove[@]}]=$imageDigest
                numImages=$((numImages-1))
            fi
        else
            break
        fi
    done <<< "$(echo $listImages | jq -rc '.[]')"

    if [ ${#listImageRemove[*]} -ne 0  ]; then
        echo "[cleanup] Clean up ${#listImageRemove[*]} images"
        imageRemove=""
        for i in ${!listImageRemove[*]}
        do
            printf "%4d: %s\n" $((i+1)) ${listImageRemove[$i]}
            imageRemove="${imageRemove}imageDigest=${listImageRemove[$i]} "
        done

        echo "[cleanup] [aws][ecs][batch-delete-image] ${repository} ${imageRemove}"
        aws ecr batch-delete-image --region $region --repository-name $repository --image-ids $imageRemove

        numImagesAfter=$(aws ecr describe-images --region $region --repository-name $repository --output json --query 'sort_by(imageDetails,& imagePushedAt)' | jq length)
        echo "[cleanup] After clean up: $numImagesAfter"
    else
        echo "[cleanup] No image to clean up"
    fi
}

while getopts ":r:i:t:n:" opt; do
case ${opt} in
    r)
        registry=${OPTARG}
        ;;
    i)
        images=${OPTARG}
        ;;
    t)
        tags=${OPTARG}
        ;;
    n)
        backup=${OPTARG}
        ;;
    \?)
        echo "Invalid option: -$OPTARG" >&2
        ;;
  esac
done

# Push the image build to aws Registry
if [ -z "${images}" ]; then
        echo "At least one image name is mandatory"
        exit 3
fi

if [ -z "${tags}" ]; then
        echo "At least one tag name is mandatory"
        exit 3
fi

if [ -z "${registry}" ]; then
        echo "You need to provide regsitry"
        exit 3
fi

if [[ -z "${backup}" || $backup == *[^[:digit:]]* ]]; then
    backup=10
    echo "[cleanup] Backup images not defined, set to default: $backup"
fi

region=$(echo "${registry}" | sed -e 's/.*\.ecr\.\([-a-z0-9]\+\)\.amazonaws\.com.*$/\1/g')

echo "[aws][ecr] get-login"
eval $(aws ecr get-login-password --region ${region} | sed 's|https://||')

for image in ${images}; do
    imageid=$(docker-compose images | awk '$1~/_'${image}'_1.*$/{print $4}')
    if [ -z "${imageid}" ]; then
    	echo "[docker-compose][images] '${image}' not found:"
    	docker-compose images
    	exit 1
    fi

    for tag in ${tags}; do
        echo "[docker][tag] ${imageid} ${registry}/${image}:${tag}"
        docker tag ${imageid} ${registry}/${image}:${tag}
        echo "[docker][push] ${registry}/${image}:${tag}"
        docker push ${registry}/${image}:${tag}
    done

    # Cleanup for each repository after push
    echo "Cleanup repository"
    cleanup "$registry/$image" $backup
done
